<!-- Add Student -->
<?php
    include ('index.php');       
?>

<section>
    <div class="container">
        <div class="row">
        <div class="col-md-12">
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <h3>Add Student Information</h3>   
                
                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="student_name">Student Name:</label>
                            <input type="text" name="student_name" class="form-control" placeholder="Enter Student Name" style="width: 350px;">
                        </div>
                    </div>

                    <div class="col-md-4">    
                        <div class="form-group">
                            <label for="address">Address:</label>
                            <input type="text" name="address" class="form-control" placeholder="Enter Address" style="width: 350px;">
                        </div>
                    </div>

                </div>    
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="age">Student's Age:</label>
                            <input type="text" name="age" class="form-control" placeholder="Enter Student's Age" style="width: 350px;">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="class</div></div></div>_id">Class ID:</label>
                            <input type="text" name="class_id" class="form-control" placeholder="Enter Class ID" style="width: 350px;">
                        </div>
                    </div>    
                </div>

                <button name="Submit" type="submit" value="Add" class="btn btn-primary">Submit</button>
                <a class="btn btn-primary" href="home.php" role="button">Show Table</a>
            </form>
        </div> 
        </div>
    </div>

    <?php
        include ('addstudent.php');       
    ?>

</section> 