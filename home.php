<link rel="stylesheet" href="../Bootstrap4/css/bootstrap.min.css">

<?php
    include('index.php');
?>

<?php
    include_once("config.php");
 
    $result = mysqli_query($mysqli, "SELECT * FROM student ORDER BY id ASC"); 
?>
 
<section>

    <h2> STUDENT TABLE</h2>

    <a class="btn btn-primary" href="addstudentdata.php" role="button">Add data</a>
    <br/><br/>
    
    <div class="table">
        <table class="table table-bordered" style="width:98%" >

            <tr bgcolor='#999a9b'>
                <td>Student ID</td>
                <td>Name</td>
                <td>Address</td>
                <td>Age</td>
                <td>Class ID</td>
                <td>Update</td>
            </tr>
            <?php            
                    while($res = mysqli_fetch_array($result)) {         

                        echo "<tr>";
                        echo "<td>".$res['id']."</td>";
                        echo "<td>".$res['student_name']."</td>";
                        echo "<td>".$res['address']."</td>";
                        echo "<td>".$res['age']."</td>";
                        echo "<td>".$res['class_id']."</td>";    
                        echo "<td><a href=\"edit.php?id=$res[id]\">Edit</a> | <a href=\"delete.php?id=$res[id]\" onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a></td>";                            
                    }
            ?>
        </table>
    </div>

</section>