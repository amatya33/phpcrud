<?php
    include('index.php');
?>

<?php
    //the database connection file
    include_once("config.php");
 
        if(isset($_POST['update']))
        {    
            $id = $_POST['id'];
            
            $student_name=$_POST['student_name'];
            $address=$_POST['address'];
            $age=$_POST['age'];
            $class_id=$_POST['class_id'];    
            
            if(empty($student_name) || empty($address) || empty($age) || empty($class_id)) {            
                echo '<script language="javascript">';
                echo 'alert("Please fill out all the fields!")';
                echo '</script>';      
            } 
            
            else {    
                //updating the table
                if($result = mysqli_query($mysqli, "UPDATE student SET student_name='$student_name', address='$address', age='$age', class_id='$class_id' WHERE id=$id")){

                    header("Location: home.php");
                }
                else{
                    echo '<script language="javascript">';
                    echo 'alert("Failed to update!")';
                    echo '</script>';
                }

                
            }
        }
?>

<?php

    $id = $_GET['id'];
 
    $result = mysqli_query($mysqli, "SELECT * FROM student WHERE id=$id");
    
    while($res = mysqli_fetch_array($result))
    {
        $student_name = $res['student_name'];
        $address = $res['address'];
        $age = $res['age'];
        $class_id = $res['class_id'];
    }
?>

<section>
    <div class="container">
        <div class="row">
        <div class="col-md-12">
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <h3>Add Student Information</h3>   
                
                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="student_name">Student Name:</label>
                            <input type="text" name="student_name" class="form-control" value="<?php echo $student_name;?>" style="width: 350px;">
                        </div>
                    </div>

                    <div class="col-md-4">    
                        <div class="form-group">
                            <label for="address">Address:</label>
                            <input type="text" name="address" class="form-control" value="<?php echo $address;?>" style="width: 350px;">
                        </div>
                    </div>

                </div>    
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="age">Student's Age:</label>
                            <input type="text" name="age" class="form-control" value="<?php echo $age;?>" style="width: 350px;">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="class</div></div></div>_id">Class ID:</label>
                            <input type="text" name="class_id" class="form-control" value="<?php echo $class_id;?>" style="width: 350px;">
                        </div>
                    </div>    
                </div>

                <input type="hidden" name="id" value=<?php echo $_GET['id'];?>>
                <button name="update" type="submit" value="Update" class="btn btn-primary">Update</button>
                <a class="btn" href="home.php" role="button">Cancel</a>
            </form>
        </div> 
        </div>
    </div>
</section>